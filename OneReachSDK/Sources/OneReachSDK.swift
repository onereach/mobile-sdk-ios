//
//  OneReachSDK.swift
//  OneReachSDK
//
//  Created by Andrey on 09/10/2018.
//

import Foundation

public class OneReachSDK {
    
    public static func startApp(appURL: String) -> AppViewController?  {
        
        guard let url = URL(string: appURL) else {
            return nil
        }
        
        let controller = AppViewController()
        let request = URLRequest(url: url)
        controller.webView.load(request)
        
        return controller
    }
}
