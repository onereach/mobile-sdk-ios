//
//  AppViewController.swift
//  OneReachSDK
//
//  Created by Andrey on 09/10/2018.
//

import WebKit

public class AppViewController: UIViewController {
    
    public lazy var webView: WKWebView = {
        
        let webView = WKWebView()
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    public lazy var eventManager: EventManager = {
        
        return EventManager(webView: webView)
    }()
    
    override public func loadView() {
        super.loadView()
        
        self.view.addSubview(webView)
        
        NSLayoutConstraint.activate([ webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                      webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                      webView.topAnchor.constraint(equalTo: view.topAnchor),
                                      webView.bottomAnchor.constraint(equalTo: view.bottomAnchor) ])
    }
}
