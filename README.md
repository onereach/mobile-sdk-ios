# OneReachSDK

[![Carthage Compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)

OneReachSDK extends WKWebview with JS bridge functionality

- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [Authors](#authors)
- [License](#license)

## Features

- [x] Feature 1
- [x] Feature 2
- [x] Feature 3

## Requirements

iOS 9.0 +

## Installation

### CocoaPods Installation

OneReachSDK is available through [CocoaPods](https://cocoapods.org).
Add the following line to your target in your `Podfile`:

```ruby
pod 'OneReachSDK', :git => 'https://gitlab.com/onereach/mobile/sdk/ios.git'

```
And run the following command:

```bash
$ pod install
```

### Carthage Installation

OneReachSDK is available through [Carthage](https://github.com/Carthage/Carthage).
Add the following line to your `Cartfile`:

```ruby
git "https://gitlab.com/onereach/mobile/sdk/ios.git" ~> 0.0.1
```

Build `OneReachSDK.framework` by running the following command:

```bash
$ carthage update
```

And drag `OneReachSDK.framework` into your project.

### Manual Installation

1. Clone OneReachSDK repository or add it as a submodule to your project repository;
2. Drag OneReachSDK.xcodeproj into your project;
3. In your project target, 'General', 'Embedded Binaries' add `OneReachSDK.framework`.

## Usage

Show examples for featur 1
Show examples for featur 2
Show examples for featur 3

## Authors

* **First First** - [Link To Persone](http://)
* **Second Second** - [Link To Persone](http://)

## License

OneReachSDK is licensed under the MIT License - see the [LICENSE](OneReachSDK/LICENSE) file for details.
