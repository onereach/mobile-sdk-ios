Pod::Spec.new do |s|
  s.name             = 'OneReachSDK'
  s.version          = '0.0.1'
  s.summary          = 'OneReachSDK'
  s.description      = 'This CocoaPod provides OneReachSDK'
  s.homepage         = 'https://gitlab.com/onereach/mobile/sdk/ios'
  s.license          = { :type => 'MIT', :file => 'OneReachSDK/LICENSE' }
  s.author           = { 'Andrey Bratsun' => 'andrey.bratsun@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/onereach/mobile/sdk/ios.git', :tag => s.version.to_s }
  s.source_files = 'OneReachSDK/Sources/*.swift'
  s.swift_version = '5.0'
  s.frameworks = 'WebKit', 'UIKit'
  s.ios.deployment_target = '9.0'
end
