//
//  EventManager.swift
//  OneReachSDK
//
//  Created by Andrey on 09/10/2018.
//

import WebKit

public class EventManager: NSObject {
    
    fileprivate static let eventHandlerName = "OneReach"
    
    public typealias EvetHandler = (_ parameters: [String: Any]?) -> Void
    
    fileprivate var eventHandlers = [String: EvetHandler]()
    
    public let webView: WKWebView
    
    public init(webView wView: WKWebView) {
        webView = wView
        
        super.init()
        
        webView.configuration.addObserver(self,
                                          forKeyPath: #keyPath(WKWebViewConfiguration.userContentController),
                                          options: [.new, .old],
                                          context: nil)
        
        webView.configuration.userContentController.add(self, name: EventManager.eventHandlerName)
    }
    
    deinit {
        webView.configuration.removeObserver(self, forKeyPath: #keyPath(WKWebViewConfiguration.userContentController))
    }
    
    override public func observeValue(forKeyPath keyPath: String?,
                                of object: Any?,
                                    change: [NSKeyValueChangeKey : Any]?,
                                    context: UnsafeMutableRawPointer?) {
        
        guard let object = object as? WKWebViewConfiguration, let keyPath = keyPath, let change = change  else {
            return
        }
        
        if object != webView.configuration || keyPath != #keyPath(WKWebViewConfiguration.userContentController) {
            return
        }
        
        if let oldController = change[.oldKey] as? WKUserContentController {
            oldController.removeScriptMessageHandler(forName: EventManager.eventHandlerName)
        }
        
        if let newController = change[.newKey] as? WKUserContentController {
            newController.add(self, name: EventManager.eventHandlerName)
        }
    }
    
    /// Register js event handler
    ///
    /// - Parameters:
    ///   - handler: closure to be triggered by event
    ///   - eventName: name of event
    public func register(handler: @escaping EvetHandler,
                         forEvent eventName:String) {
        
        eventHandlers[eventName] = handler
    }
    
    /// Unregister js event handler
    ///
    /// - Parameter eventName: name of event
    public func unregisterHandler(forEvent eventName:String) {
        
        eventHandlers[eventName] = nil
    }
    
    /// Send event to js
    ///
    /// - Parameters:
    ///   - eventName: name of event
    ///   - parameters: js parameters
    public func send(event eventName: String,
                     withParameters parameters: [String: Any]? = nil) {
        
        var functionStr: String
        
        if let parameters = parameters {
            
            guard let _data = try? JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions()),
                let parametersStr = String(data: _data, encoding: .utf8) else {
                    
                    return
            }
            
            functionStr = "handleEvent('\(eventName)',\(parametersStr))"
        }
        else {
            
            functionStr = "handleEvent('\(eventName)')"
        }
        
        webView.evaluateJavaScript(functionStr)
    }
}

extension EventManager: WKScriptMessageHandler {
    
    public func userContentController(_ userContentController: WKUserContentController,
                                      didReceive message: WKScriptMessage) {
        
        if message.name != EventManager.eventHandlerName {
            return
        }
        
        guard let body = message.body as? [String: Any],
            let event = Event(dictionary: body) else {
                return
        }
        
        eventHandlers[event.name]?(event.parameters)
        
        NotificationCenter.default.post(name: NSNotification.Name.EventManager.DidReceiveEvent, object: self, userInfo: body)
    }
}
