//
//  Notifications.swift
//  OneReachSDK
//
//  Created by Andrey on 09/10/2018.
//

import Foundation

extension Notification.Name {

    public struct EventManager {

        public static let DidReceiveEvent = Notification.Name(rawValue: "OneReachEventManagerDidReceiveEvent")
    }
}
