var eventHandlers = {};

<!--     Register native event handler-->
<!--     Parameter handler - function to be triggered by event-->
<!--     Parameter eventName - name of event-->
function registerEventHandler(eventName, handler) {
    eventHandlers[eventName] = handler;
};


<!--     Unregister native event handler-->
<!--     Parameter eventName - name of event-->
function unregisterEventHandler(eventName) {
    eventHandlers[eventName] = nil;
};


<!--    Invoked when a event is received from a native-->
<!--    Parameter eventName - name of received event-->
<!--    Parameter parameters - event parameters-->
function handleEvent(eventName, parameters) {
    eventHandlers[eventName](parameters);
};


<!--    Send event to native-->
<!--    Parameter eventName - name of event-->
<!--    Parameter parameters - native handler parameters.-->
function sendEvent(eventName, parameters) {
    window.webkit.messageHandlers.OneReach.postMessage({"name":eventName, "parameters":parameters});
};
