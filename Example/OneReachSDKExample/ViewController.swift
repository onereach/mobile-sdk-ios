//
//  ViewController.swift
//  OneReachSDK
//
//  Created by Andrey Bratsun on 10/04/2018.
//  Copyright (c) 2018 Andrey Bratsun. All rights reserved.
//

import UIKit
import WebKit
import OneReachSDK

class ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var toolbarBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var toolbar: UIToolbar!
    
    lazy var eventManager: EventManager = {
        return EventManager(webView: webView)
    }()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = Bundle.main.url(forResource: "webview", withExtension:"html")!
        webView.load(URLRequest(url: url))
        
        webView.uiDelegate = self
        
        eventManager.register(handler: { (params) in
            
            if let message = params?["message"] as? String {
                let controller = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }
            
        }, forEvent: "showMessage")
        
        eventManager.register(handler: { (_) in
            
            self.toolbarBottomConstraint.constant = -self.toolbar.frame.size.height
            
            UIView.animate(withDuration: 0.3, animations: {
                self.toolbar.alpha = 0
                self.view.layoutIfNeeded()
            })
            
        }, forEvent: "hideToolBar")
        
        eventManager.register(handler: { (_) in
            
            self.toolbarBottomConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.toolbar.alpha = 1
                self.view.layoutIfNeeded()
            })
            
        }, forEvent: "showToolBar")
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReciveNotification(note:)), name: NSNotification.Name.EventManager.DidReceiveEvent, object: eventManager)
    }
    
    @IBAction func showJSAlert() {
        eventManager.send(event: "showAlert", withParameters: ["message":"JS Alert"])
    }
    
    @IBAction func chengeTextColor() {
        eventManager.send(event: "chengeTextColor")
    }
    
    @objc func didReciveNotification(note: Notification) {
        print("Did recive notification from JS  ====> \n \(String(describing: note.userInfo))")
    }
}

extension ViewController: WKUIDelegate {
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler()
        }))
        
        present(alertController, animated: true, completion: nil)
    }
}

