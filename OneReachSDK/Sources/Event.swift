//
//  Event.swift
//  OneReachSDK
//
//  Created by Andrey on 09/10/2018.
//

import Foundation

/*!
 JS event abstracion. It probably will be removed
 */
public struct Event {
    
    private enum EventKey: String {
        case name = "name"
        case parameters = "parameters"
    }
    
    let name: String
    let parameters: [String: Any]?
    
    init?(dictionary: [String: Any]) {
        guard let eventName = dictionary[EventKey.name.rawValue] as? String else {
            return nil
        }
        
        name = eventName
        parameters = dictionary[EventKey.parameters.rawValue] as? [String: Any]
    }
}
